package com.bibao.boot.springbootmvcthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.bibao.boot")
public class SpringBootMvcThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMvcThymeleafApplication.class, args);
	}
}
